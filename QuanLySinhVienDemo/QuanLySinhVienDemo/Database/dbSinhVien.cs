namespace QuanLySinhVienDemo.Database
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dbSinhVien : DbContext
    {
        public dbSinhVien()
            : base("name=dbSinhVien")
        {
        }

        public virtual DbSet<LopHoc> LopHocs { get; set; }
        public virtual DbSet<SinhVien> SinhViens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LopHoc>()
                .HasMany(e => e.SinhViens)
                .WithRequired(e => e.LopHoc)
                .WillCascadeOnDelete(false);
        }
    }
}
